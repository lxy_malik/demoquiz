import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from './../http-service.service';

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.css']
})
export class TestPageComponent implements OnInit {
  /*
    Uncomment the data arrays if you don't want to start express server
  */

  // quizQuestions = [
  //   {
  //     q_num: 1,
  //     q_text: "What is Angular?",
  //     opt_a: 'Library',
  //     opt_b: 'Language',
  //     opt_c: 'Framework',
  //     opt_d: 'None of the above'
  //   },
  //   {
  //     q_num: 2,
  //     q_text: "What is JavaScript?",
  //     opt_a: 'Library',
  //     opt_b: 'Language',
  //     opt_c: 'Framework',
  //     opt_d: 'None of the above'
  //   },
  //   {
  //     q_num: 3,
  //     q_text: "What is TypeScript?",
  //     opt_a: 'Library',
  //     opt_b: 'Language',
  //     opt_c: 'Framework',
  //     opt_d: 'None of the above'
  //   },
  // ];
  // quizAnswers = ['c', 'b', 'b'];

  quizQuestions = [];
  quizAnswers = [];
  currentQues = {};
  responseArray = [];
  response: any;
  isTestReport = false;
  correctAns = 0;
  wrongAns = 0;

  constructor(
    private _http: HttpServiceService,
  ) { }

  ngOnInit() {
    /*
      Comment these function calls and uncomment question data assignment logic
    */
    this.getQuizFromServer();
    this.getSolutionFromServer();
    // this.currentQues = this.quizQuestions[0];
  }

  saveNext() {
    this.responseArray[this.currentQues['q_num'] - 1] = this.response;
    this.response = '';
    if (this.currentQues['q_num'] < this.quizQuestions.length) {
      this.currentQues = this.quizQuestions[this.currentQues['q_num']];
    }
  }

  goToPrevious() {
    this.currentQues = this.quizQuestions[this.currentQues['q_num'] - 2];
    this.response = this.responseArray[this.currentQues['q_num'] - 1];

    setTimeout(() => {
      if (this.response == 'a') {
          (<HTMLInputElement> document.getElementById("color-1")).checked = true;
      }
      if (this.response == 'b') {
          (<HTMLInputElement> document.getElementById("color-2")).checked = true;
      }
      if (this.response == 'c') {
          (<HTMLInputElement> document.getElementById("color-3")).checked = true;
      }
      if (this.response == 'd') {
          (<HTMLInputElement> document.getElementById("color-4")).checked = true;
      }
  }, 100);

  }

  submitQuiz() {
    this.responseArray.push(this.response);
    for (let i = 0; i < this.quizQuestions.length; i++) {
      this.responseArray[i] === this.quizAnswers[i] ? this.correctAns++ : this.wrongAns++;
    }
    this.isTestReport = true;
  }

  resetQuiz() {
    this.currentQues = this.quizQuestions[0];
    this.response = '';
    this.responseArray = [];
    this.isTestReport = false;
    this.wrongAns = 0;
    this.correctAns = 0;
  }

  getQuizFromServer() {
    this._http.httpPost('getQuizQuestions', {}).subscribe((res) => {
      console.log(res);
      if (res['code'] == 1) {
        this.quizQuestions = res['QUESTIONS'];
        this.currentQues = this.quizQuestions[0];
      } else {
        // do something
      }

    });
  }

  getSolutionFromServer() {
    this._http.httpPost('getQuizSolution', {}).subscribe((res) => {
      console.log(res);
      if (res['code'] == 1) {
        this.quizAnswers = res['ANSWERS'];
      } else {
        // do something
      }
    });
  }

}
