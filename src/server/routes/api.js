const express = require('express');
const router = express.Router();

/* All data should be in database. e.g. MongoDB with mongoose */
quizQuestions = [
  {
    q_num: 1,
    q_text: "What is Angular?",
    opt_a: 'Library',
    opt_b: 'Language',
    opt_c: 'Framework',
    opt_d: 'None of the above'
  },
  {
    q_num: 2,
    q_text: "What is JavaScript?",
    opt_a: 'Library',
    opt_b: 'Language',
    opt_c: 'Framework',
    opt_d: 'None of the above'
  },
  {
    q_num: 3,
    q_text: "What is TypeScript?",
    opt_a: 'Library',
    opt_b: 'Language',
    opt_c: 'Framework',
    opt_d: 'None of the above'
  },
];

const quizAnswers = ['c','b','b'];

router.post('/getQuizQuestions', (req, res) => {
  res.status(200).json({
    code: 1,
    QUESTIONS: quizQuestions
  });
});

router.post('/getQuizSolution', (req, res) => {
  res.status(200).json({
    code: 1,
    ANSWERS: quizAnswers
  });
});

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

module.exports = router;
